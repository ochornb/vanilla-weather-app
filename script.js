const notifcationElement = document.querySelector('.notification');
const iconElement = document.querySelector('.weather-icon');
const tempElement =document.querySelector('.temperature-value p');
const descElement = document.querySelector('.temperature-description p');
const locationElement = document.querySelector('.location p');

const searchButton = document.getElementById('search-button');
let searchInput = document.getElementById('search-terms');
const clearButton = document.getElementById('clear-button');
const unitButton = document.getElementById('convert-slider');
const unitImperial = document.getElementById('imperial');
const unitMetric = document.getElementById('metric');
const unitSlider = document.getElementById('slider');
let unitAbbr =  document.getElementById('temp-unit');
let keyInput = document.getElementById('key-input');

// get user input, check validation, call getWeather if input is valid
searchButton.addEventListener('click', function(event){
    event.preventDefault();
    if(!searchInput.checkValidity() || !keyInput.checkValidity()){ // input on the form is inavlid!
        return;                                                   // don't do anything
    }
    else {                                                       // otherwise
        searchWeather();                                         // call search weather
    }
});

// clear input button
// clear the search bar
// call func to format the page
clearButton.addEventListener('click', function(event) {
    event.preventDefault();
    clearInput();
});

unitButton.addEventListener('click', function(event) {
    moveSlider();
    searchWeather();
    
});

// put all of search button into a function
// so we can call it in search button click event as well as the unit button
function searchWeather(){
    let unitActive = document.getElementsByClassName('active-unit')[0].id;  // grab which unit is currently active (C or F)

    if(searchInput.getAttribute('required') === null){                      // if required has been removed
        searchInput.setAttribute('required','true');                        // add required back on
    }
    else if (keyInput.getAttribute('required') === null){
        keyInput.setAttribute('required', 'true');
    }

    let searchTerms = '';
    searchTerms = cleanInput(searchInput.value);                           // get the city and country code, remove extra whitespaces

    let key = keyInput.value;

    getWeather(searchTerms[0], checkIfState(searchTerms[1]), unitActive, key);           // send info to the api request
   
}

// this api accepts country codes and not state codes, unfortunately
// if user inputs a state code, change it to USA
function checkIfState(searchCode){
    let stateList = ["AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","GU","HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME","MH","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH","OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI","VT","WA","WI","WV","WY"];

    if(stateList.indexOf(searchCode.toUpperCase()) === -1){
        return searchCode;
    }
    else{
        return "USA";
    }
}
// move slider button to active unit of measurement
function moveSlider() {
    // check to see if the measurement is active
    // then toggle on click
    if(unitMetric.className === 'active-unit'){
        unitSlider.style.left = '50%';
        unitImperial.setAttribute('class','active-unit');
        unitMetric.removeAttribute('class');
        unitAbbr.innerHTML = 'F';
    }else {
        unitSlider.style.left = '0px';
        unitMetric.setAttribute('class', 'active-unit');
        unitImperial.removeAttribute('class');
        unitAbbr.innerHTML= 'C';
    }
}

// break user input into parts 
// and save it into an array
function cleanInput(searchterms) {
    let comma = searchterms.indexOf(',')
    // might need a case for a city with two words in their name
    let city = searchterms.substring(0, comma).trim();
    let country = searchterms.substring(comma+1, searchterms.length).trim();
    let searchTermArray =[city , country];
    return searchTermArray;
};

// clear search bar input and format weather information
// change clear button to search button
function clearInput(){
    searchInput.value = '';
    keyInput.value = '';

    currentDay.innerHTML = 
    `
    <div class ='current-day'>
        <h2 class ='location'> - </h2>
        <p class = 'weather-description'> - </p>
        <div class ='weather-icon'><img src='https://openweathermap.org/img/wn/10d@2x.png'/></div>
        <h1 class ='temperature-value'>- ° <span id = 'temp-unit'>F</span></h1>
    </div><!--current day-->
    `
    // clear fiveDay
    fiveDay.innerHTML = '';

    //populate fiveDay with empties
    for (var i = 0; i < 6; i++ ){
        fiveDay.innerHTML += 
        `
            <div class = 'day'>
                <p class = 'day-title'>-</p>
                <img class = 'small-icon' src='https://openweathermap.org/img/wn/10d.png'/>
                <p class = 'temp-max'>-</p>
                <p class ='temp-min'>-</p>
            </div>
        `
    }
    // reset required in input field
    // or else it'll glow red bc it senses the search box is empty
    // turn it back on when the user clicks the search button again
    searchInput.removeAttribute('required');  //turn it off
    keyInput.removeAttribute('required');
}

// call to capitalize today's weather description
function captializeDescription(description) {
    var space = description.indexOf(' ');
    var newDesc = '';

    if (space === -1){          // No Spaces, one word 'Cloudy'
        newDesc = description[0].toUpperCase() + description.substring(1, description.length);
    } else {                   // otherwise it's two words 'Light Rain'
        description[space+1] = description[space+1].toUpperCase();
        newDesc = description[0].toUpperCase() + description.substring(1, space) + ' ' + description[space+1].toUpperCase() + description.substring(space+2, description.length);
    }
    return newDesc;
}

const fiveDay = document.querySelector('.five-day');
const currentDay = document.querySelector('.current-day');

function getWeather(city, countryCode, unit, key){

    let api = `https://api.openweathermap.org/data/2.5/forecast?q=${city},${countryCode}&units=${unit}&appid=${key}&mode=json`;
    
    fetch(api)
        .then( function(response) {
            let data = response.json();
            return data;
        })
        .then( function(data){
            let tempMin = Math.floor(data.list[0].main.temp_min);
            let tempMax = Math.floor(data.list[0].main.temp_max);
            // gather today's weather info and display it top and center of the app
            currentDay.innerHTML =
            `
            <h2 class = 'location'>${data.city.name}</h2>
            <p class = 'weather-description'>${captializeDescription(data.list[0].weather[0].description)}</p>
            <div class = 'weather-icon'><img src='http://openweathermap.org/img/wn/${data.list[0].weather[0].icon}@2x.png'/></div>
            <h1 class = 'temperature-value'>${Math.floor(data.list[0].main.temp)}° <span id = 'temp-unit'>${unitAbbr.innerHTML}</span></h1>
            `
            // gather today's weather info and display it in a row
            fiveDay.innerHTML =
            `
                <div class ='day today'>
                    <p class = 'day-title'>Today</p>
                    <img src='http://openweathermap.org/img/wn/${data.list[0].weather[0].icon}.png'/>
                    <p class = 'temp-max'>${Math.floor(tempMax)}</p>
                    <p class = 'temp-min'>${Math.floor(tempMin)}</p>
                </div>
            `
            // gather the other day's info, at roughly 3pm each day, display it in a row
            for (var i = 1; i < 6; i++){
                var inc = (i*8)-1;              //increment by 8 to grab max and low temp for the day
                var date = new Intl.DateTimeFormat('en-US', {weekday:'long'}).format(Date.parse(data.list[inc].dt_txt));
                tempMin = Math.floor(data.list[inc].main.temp_min);
                tempMax = Math.floor(data.list[inc].main.temp_max);

                // Note: min and max temps are calculated at the time of the forecast meaning max and min temps will be very similar on days that are not today
                // this is because it's a 5 day / 3 hour forecast
                // openweathermap has 16 day forecast that may solve this issue but it requires a sub $$$

                fiveDay.innerHTML +=
                `<div class = 'day'>
                    <p class = 'day-title'>${date}</p>
                    <img class = 'small-icon' src='https://openweathermap.org/img/wn/${data.list[inc].weather[0].icon}.png'/>
                    <p class = 'temp-max'>${tempMax}</p>
                    <p class ='temp-min'>${tempMin}</p>
                </div>
                `;
            }
        })
}
