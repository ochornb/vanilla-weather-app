# Vanilla HTML, CSS, and JavaScript Weather App with OpenWeatherWeatherMap API
- - - -
View it [here](https://ochornb.bitbucket.io/weatherapp/)
----
![preview](https://bitbucket.org/ochornb/ochornb.bitbucket.io/raw/2f18aeeba44caace91b9e7f3edbcdb31ce709939/portfolio/imgs/weather_5_day.JPG)

## **Tools used**
- - - -
* HTML, CSS, JavaScript
* Bitbucket
* [OpenWeatherMap API](https://openweathermap.org/)

## **Summary**
- - - -
As a developer intern applicant I was assigned a Figure It Out task: create an app that displays a five-day forecast for a given city and state. 

I completed this project in a week and learned how to pull and display information from a JSON file returned from an API. I used Flexbox to make the webpage responsive.

I didn't know how to safely integrate the API token as that's generally a [bad idea](https://latesthackingnews.com/2020/01/04/starbucks-exposed-an-api-key-in-github-public-repository/) to put it in a public repo, so I made a text input that accepts an OpenWeatherMap token.

## **Author**
- - - -
* **Olivia Hornback** - *Front End Web Developer* - [Website](https://ochornb.bitbucket.io/) | [LinkedIn](https://linkedin.com/in/oliviahornback)



